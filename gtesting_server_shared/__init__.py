__version_info__ = ('0', '0', '5')
__version__ = '.'.join(__version_info__)
__all__ = (
        "Test",
        "TestSet",
        "TestReport",
        "SubmitionReport"
    )


from uuid import uuid1
from enum import Enum
from typing import List

from dataclasses import field
from marshmallow_dataclass import dataclass


def id_factory() -> str:
    return str(uuid1())


@dataclass
class Test:

    input: List[str]
    output: List[str]


@dataclass
class TestSet:

    tests: List[Test]   = field(default_factory=list)
    time_limit: float   = 1.0
    _id: str            = field(default_factory=id_factory)


@dataclass
class TestReport:

    class Verdict(Enum):

        OK = "Success"
        RE = "Runtime Error"
        WA = "Wrong answer"
        TL = "Time Limit"

    verdict: Verdict
    messages: List[str] = field(default_factory=list)


@dataclass
class SubmitionReport:

    class Status(Enum):

        WAITING             = "Waiting"
        COMPILATION         = "Compilation"
        COMPILATION_FAILED  = "Compilation failed"
        RUNNING             = "Running"
        FAILED              = "Failed"
        FINISHED            = "Finished"

    submition_id: str
    status: Status              = Status.WAITING
    reports: List[TestReport]   = field(default_factory=list)
    messages: List[str]         = field(default_factory=list)
