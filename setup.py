from setuptools import setup

setup(
    name="gtesting-server-shared",
    version="0.0.5",
    packages=["gtesting_server_shared"],
    install_requires=["marshmallow-dataclass[enum]>=8.5.0"])
